<?php

require_once "../controladores/impuestos.controlador.php";
require_once "../modelos/impuestos.modelo.php";

class AjaxImpuesto{

	/*=============================================
	EDITAR IMPUESTO
	=============================================*/	

	public $idImpuesto;

	public function ajaxEditarImpuesto(){

		$item = "id_impuesto";
		$valor = $this->idImpuesto;

		$respuesta = ControladorImpuestos::ctrMostrarImpuestos($item, $valor);

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR CATEGORÍA
=============================================*/	
if(isset($_POST["idImpuesto"])){

	$impuesto = new AjaxImpuesto();
	$impuesto -> idImpuesto = $_POST["idImpuesto"];
	$impuesto -> ajaxEditarImpuesto();
}

<?php

require_once "../controladores/unidad.medida.controlador.php";
require_once "../modelos/unidad.medida.modelo.php";

class AjaxUnidadMedida{

	/*=============================================
	EDITAR UNIDAD DE MEDIDA
	=============================================*/	

	public $idUnidadMedida;

	public function ajaxEditarUnidadMedida(){

		$item = "id_unidad_medida";
		$valor = $this->idUnidadMedida;

		$respuesta = ControladorUnidadMedida::ctrMostrarUnidadMedida($item, $valor);

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR CATEGORÍA
=============================================*/	
if(isset($_POST["idUnidadMedida"])){

	$unidad_medida = new AjaxUnidadMedida();
	$unidad_medida -> idUnidadMedida = $_POST["idUnidadMedida"];
	$unidad_medida -> ajaxEditarUnidadMedida();
	
}

<?php

/*Controlador de Unidades de Medida*/ 
class ControladorUnidadMedida{

/*Mostrar Unidades de Medida*/ 

static public function ctrMostrarUnidadMedida($item, $valor){

    $tabla = "unidad_medida";

	$respuesta = ModeloUnidadMedida::mdlMostrarUnidadMedida($tabla, $item, $valor);
    
	return $respuesta;

}

/*=============================================
	CREAR UNIDAD DE MEDIDA
	=============================================*/

	static public function ctrCrearUnidadMedida(){

		if(isset($_POST["descr_unidad_medida"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["descr_unidad_medida"])){

				$tabla = "unidad_medida";

                $datos = array("cod_unidad_medida" => $_POST["cod_unidad_medida"],
							   "descr_unidad_medida" => $_POST["descr_unidad_medida"],
                               "usuario_registro" => $_SESSION["id"]);

					$respuesta = ModeloUnidadMedida::mdlIngresarUnidadMedida($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La unidad de medida ha sido guardada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "unidad-medida";

									}
								})

					</script>';

				}else{

					echo'<script>

					swal({
						  type: "error",
						  title: "' . $respuesta . '",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  })

					</script>';


				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La unidad de medida no puede ser vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "unidad-medida";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	EDITAR UNDIAD DE MEDIDA
	=============================================*/

	static public function ctrEditarUnidadMedida(){

		

		if(isset($_POST["edit_descr_unidad_medida"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["edit_descr_unidad_medida"])){

				$tabla = "unidad_medida";

				$datos = array("descr_unidad_medida"=>$_POST["edit_descr_unidad_medida"],
							   "id_unidad_medida"=>$_POST["edit_id_unidad_medida"],
							   "cod_unidad_medida"=>$_POST["edit_cod_unidad_medida"]);

				$respuesta = ModeloUnidadMedida::mdlEditarUnidadMedida($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La unidad de medida ha sido editado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "unidad-medida";

									}
								})

					</script>';

				}else{

					echo'<script>

					swal({
						  type: "error",
						  title: "' . $respuesta . '",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  })

					</script>';


				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La unidad de medida no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "unidad_medida";

							}
						})

			  	</script>';

			}

		}

	}


	/*=============================================
	BORRAR UNIDAD DE MEDIDA
	=============================================*/

	static public function ctrBorrarUnidadMedida(){

		if(isset($_GET["idUnidadMedida"])){

			$tabla ="unidad_medida";
			$datos = $_GET["idUnidadMedida"];

			$respuesta = ModeloUnidadMedida::mdlBorrarUnidadMedida($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "La unidad de medida ha sido borrada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "unidad-medida";

									}
								})

					</script>';
			}
		}
		
	}





}
<?php

/*Controlador de impuestos*/ 
class ControladorImpuestos{

    /*=============================================
	CREAR IMPUESTO
	=============================================*/

	static public function ctrCrearImpuesto(){

		if(isset($_POST["valor_impuesto"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["valor_impuesto"])){

				$tabla = "impuestos";

                $datos = array("cod_impuesto" => $_POST["cod_impuesto"],
							   "descr_impuesto" => $_POST["descr_impuesto"],
							   "valor_impuesto" => $_POST["valor_impuesto"],
                               "usuario_registro" => $_SESSION["id"]);
				
				$respuesta = ModeloImpuestos::mdlIngresarImpuesto($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El impuesto ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "impuestos";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El impuesto no puede ser vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "impuestos";

							}
						})

			  	</script>';

			}

		}

	}



	/*=============================================
	EDITAR IMPUESTOS
	=============================================*/

	static public function ctrEditarImpuesto(){

		if(isset($_POST["edit_descr_impuesto"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["edit_descr_impuesto"])){

				$tabla = "impuestos";

				$datos = array("descr_impuesto"=>$_POST["edit_descr_impuesto"],
							   "id_impuesto"=>$_POST["edit_id_impuesto"],
							   "cod_impuesto"=>$_POST["edit_cod_impuesto"]);

				$respuesta = ModeloImpuestos::mdlEditarImpuesto($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El impuesto ha sido editado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "impuestos";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El impuesto no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "impuestos";

							}
						})

			  	</script>';

			}

		}

	}


/*Mostrar impuestos*/ 

static public function ctrMostrarImpuestos($item, $valor){

    $tabla = "impuestos";

    $respuesta = ModeloImpuestos::mdlMostrarImpuestos($tabla, $item, $valor);

    return $respuesta;

}





}
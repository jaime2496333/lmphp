<?php

if($_SESSION["perfil"] != "Administrador"){

    echo '<script>
  
      window.location = "inicio";
  
    </script>';
  
    return;
  
  }

  ?>

<div class="content-wrapper">

<section class="content-header">
  
  <h1>
    
    Administrar impuestos
  
  </h1>

  <ol class="breadcrumb">
    
    <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
    
    <li class="active">Administrar impuestos</li>
  
  </ol>

</section>

<section class="content">

  <div class="box">

    <div class="box-header with-border">

      <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarImpuesto">
        
        Agregar impuesto

      </button>

    </div>

    <div class="box-body">
      
     <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
       
      <thead>
       
       <tr>
         
         <th style="width:10px">#</th>
         <th>Abreviación</th>
         <th>Impuesto</th>
         <th>Valor</th>
         <th>Acciones</th>

       </tr> 

      </thead>

      <tbody>

      <?php

        $item = null;
        $valor = null;

        $impuestos = ControladorImpuestos::ctrMostrarImpuestos($item, $valor);

        foreach ($impuestos as $key => $value) {
         
          echo ' <tr>

                  <td>'.($key+1).'</td>

                  <td class="text-uppercase">'.$value["cod_impuesto"].'</td>

                  <td class="text-uppercase">'.$value["descr_impuesto"].'</td>

                  <td class="text-uppercase">'.number_format($value["valor_impuesto"],2).'</td>

                  <td>

                    <div class="btn-group">
                        
                      <button class="btn btn-warning btnEditarImpuesto" idImpuesto="'.$value["id_impuesto"].'" data-toggle="modal" data-target="#modalEditarImpuesto"><i class="fa fa-pencil"></i></button>';

                      if($_SESSION["perfil"] == "Administrador"){

                        echo '<button class="btn btn-danger btnEliminarImpuesto" idImpuesto="'.$value["id_impuesto"].'"><i class="fa fa-times"></i></button>';

                      }

                    echo '</div>  

                  </td>

                </tr>';
        }

      ?>

      </tbody>

     </table>

    </div>

  </div>

</section>

</div>

<!--=====================================
MODAL AGREGAR IMPUESTO
======================================-->

<div id="modalAgregarImpuesto" class="modal fade" role="dialog">

<div class="modal-dialog">

  <div class="modal-content">

    <form role="form" method="post">

      <!--=====================================
      CABEZA DEL MODAL
      ======================================-->

      <div class="modal-header" style="background:#3c8dbc; color:white">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar Impuesto</h4>

      </div>

      <!--=====================================
      CUERPO DEL MODAL
      ======================================-->

      <div class="modal-body">

        <div class="box-body">

        <!-- ENTRADA PARA EL CODIGO DEL IMPUESTO -->

        <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input maxlength="20" type="text" class="form-control input-lg" name="cod_impuesto" placeholder="Código Impuesto" required>

            </div>

          </div>
       

          <!-- ENTRADA PARA EL NOMBRE DEL IMPUESTO -->
          
          <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input maxlength="20" type="text" class="form-control input-lg" name="descr_impuesto" placeholder="Ingresar Impuesto" required>

            </div>

          </div>

            <!-- ENTRADA PARA VALOR IMPUESTO -->

            <div class="form-group">
              
              <div class="input-group">
              
                <span class="input-group-addon"><i class="fa fa-check"></i></span> 

                <input type="number" class="form-control input-lg" name="valor_impuesto" min="0" placeholder="Valor Impuesto" required>

              </div>

            </div>



        </div>

      </div>

      <!--=====================================
      PIE DEL MODAL
      ======================================-->

      <div class="modal-footer">

        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        <button type="submit" class="btn btn-primary">Guardar impuesto</button>

      </div>

      <?php

        $crearImpuesto = new ControladorImpuestos();
        $crearImpuesto -> ctrCrearImpuesto();

      ?>

    </form>

  </div>

</div>

</div>

<!--=====================================
MODAL EDITAR IMPUESTO
======================================-->

<div id="modalEditarImpuesto" class="modal fade" role="dialog">

<div class="modal-dialog">

  <div class="modal-content">

    <form role="form" method="post">

      <!--=====================================
      CABEZA DEL MODAL
      ======================================-->

      <div class="modal-header" style="background:#3c8dbc; color:white">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Editar Impuesto</h4>

      </div>

      <!--=====================================
      CUERPO DEL MODAL
      ======================================-->

      <div class="modal-body">

        <div class="box-body">

        <!-- ENTRADA PARA EL CODIGO IMPUESTO -->
          
        <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input type="text" class="form-control input-lg" name="edit_cod_impuesto" id="edit_cod_impuesto" required>

               <input type="hidden"  name="edit_id_impuesto" id="edit_id_impuesto" required>

            </div>
        </div>

          <!-- ENTRADA PARA EL NOMBRE -->
          
        <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input type="text" class="form-control input-lg" name="edit_descr_impuesto" id="edit_descr_impuesto" required>

               
            </div>

        </div>

        
      <!-- ENTRADA PARA VALOR NUMERICO -->
      <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-check"></i></span> 

              <input  type="text" class="form-control input-lg" name="edit_valor_impuesto" id="edit_valor_impuesto" min="0" placeholder="Valor Impuesto" required>

               
            </div>

        </div>


      <!-- -->    
        </div>        

      </div>

      <!--=====================================
      PIE DEL MODAL
      ======================================-->

      <div class="modal-footer">

        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        <button type="submit" class="btn btn-primary">Guardar cambios</button>

      </div>

    <?php

        $editarImpuesto = new ControladorImpuestos();
        $editarImpuesto -> ctrEditarImpuesto();

      ?> 

    </form>

  </div>

</div>

</div>

<?php

$borrarImpuesto = new ControladorImpuestos();
$borrarImpuesto -> ctrBorrarImpuesto();

?>


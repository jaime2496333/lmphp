<?php

if($_SESSION["perfil"] != "Administrador"){

    echo '<script>
  
      window.location = "inicio";
  
    </script>';
  
    return;
  
  }

  ?>

<div class="content-wrapper">

<section class="content-header">
  
  <h1>
    
    Administrar Unidad de Medida
  
  </h1>

  <ol class="breadcrumb">
    
    <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
    
    <li class="active">Administrar Unidad de Medida</li>
  
  </ol>

</section>

<section class="content">

  <div class="box">

    <div class="box-header with-border">

      <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUnidadMedida">
        
        Agregar Unidad de Medida

      </button>

    </div>

    <div class="box-body">
      
     <table class="table table-bordered table-striped dt-responsive tablas" width="100%">
       
      <thead>
       
       <tr>
         
         <th style="width:10px">#</th>
         <th>Cod. UM</th>
         <th>Descripción</th>
      
        </tr> 

      </thead>

      <tbody>

      <?php

        $item = null;
        $valor = null;

        $unidad_medida = ControladorUnidadMedida::ctrMostrarUnidadMedida($item, $valor);

        foreach ($unidad_medida as $key => $value) {
         
          echo ' <tr>

                  <td>'.($key+1).'</td>

                  <td class="text-uppercase">'.$value["cod_unidad_medida"].'</td>

                  <td class="text-uppercase">'.$value["descr_unidad_medida"].'</td>

                  <td>

                    <div class="btn-group">
                        
                      <button class="btn btn-warning btnEditarUnidadMedida" idUnidadMedida="'.$value["id_unidad_medida"].'" data-toggle="modal" data-target="#modalEditarUnidadMedida"><i class="fa fa-pencil"></i></button>';

                      if($_SESSION["perfil"] == "Administrador"){

                        echo '<button class="btn btn-danger btnEliminarUnidadMedida" idUnidadMedida="'.$value["id_unidad_medida"].'"><i class="fa fa-times"></i></button>';

                      }

                    echo '</div>  

                  </td>

                </tr>';
        }

      ?>

      </tbody>

     </table>

    </div>

  </div>

</section>

</div>

<!--=====================================
MODAL AGREGAR UNIDAD MEDIDA
======================================-->

<div id="modalAgregarUnidadMedida" class="modal fade" role="dialog">

<div class="modal-dialog">

  <div class="modal-content">

    <form role="form" method="post">

      <!--=====================================
      CABEZA DEL MODAL
      ======================================-->

      <div class="modal-header" style="background:#3c8dbc; color:white">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar Unidad de Medida</h4>

      </div>

      <!--=====================================
      CUERPO DEL MODAL
      ======================================-->

      <div class="modal-body">

        <div class="box-body">

        <!-- ENTRADA PARA EL CODIGO DE LA UNIDAD DE MEDIDA -->

        <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input maxlength="20" type="text" class="form-control input-lg" name="cod_unidad_medida" placeholder="Código UM" required>

            </div>

          </div>
        <!-- CODIGO DE LA UNIDAD DE MEDIDA -->
       

          <!-- UNIDAD DE MEDIDA -->
          
          <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input maxlength="20" type="text" class="form-control input-lg" name="descr_unidad_medida" placeholder="Ingresar Unidad de Medida" required>

            </div>

          </div>

        <!--FINALIZA LA UNIDAD DE MEDIDA-->    

        </div>

      </div>

      <!--=====================================
      PIE DEL MODAL
      ======================================-->

      <div class="modal-footer">

        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        <button type="submit" class="btn btn-primary">Guardar Unidad de Medida</button>

      </div>

      <?php

        $crearUnidadMedida = new ControladorUnidadMedida();
        $crearUnidadMedida -> ctrCrearUnidadMedida();

      ?>

    </form>

  </div>

</div>

</div>

<!--=====================================
MODAL EDITAR UNIDAD DE MEDIDA
======================================-->

<div id="modalEditarUnidadMedida" class="modal fade" role="dialog">

<div class="modal-dialog">

  <div class="modal-content">

    <form role="form" method="post">

      <!--=====================================
      CABEZA DEL MODAL
      ======================================-->

      <div class="modal-header" style="background:#3c8dbc; color:white">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Editar Unidad de Medida</h4>

      </div>

      <!--=====================================
      CUERPO DEL MODAL
      ======================================-->

      <div class="modal-body">

        <div class="box-body">

         <!-- ENTRADA PARA EL CODIGO DE LA UNIDAD DE MEDIDA -->

         <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input type="hidden"  name="edit_id_unidad_medida" id="edit_id_unidad_medida" required>

              <input maxlength="20" type="text" class="form-control input-lg" name="edit_cod_unidad_medida" id="edit_cod_unidad_medida" placeholder="Código UM" required>

            </div>

          </div>
        <!-- CODIGO DE LA UNIDAD DE MEDIDA -->
       

          <!-- UNIDAD DE MEDIDA -->
          
          <div class="form-group">
            
            <div class="input-group">
            
              <span class="input-group-addon"><i class="fa fa-th"></i></span> 

              <input maxlength="20" type="text" class="form-control input-lg" name="edit_descr_unidad_medida" id="edit_descr_unidad_medida" placeholder="Ingresar Unidad de Medida" required>

            </div>

          </div>

        <!--FINALIZA LA UNIDAD DE MEDIDA-->    

      </div>        

      </div>

      <!--=====================================
      PIE DEL MODAL
      ======================================-->

      <div class="modal-footer">

        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>

        <button type="submit" class="btn btn-primary">Guardar cambios</button>

      </div>

    <?php

        $editarUnidadMedida = new ControladorUnidadMedida();
        $editarUnidadMedida -> ctrEditarUnidadMedida();

      ?> 

    </form>

  </div>

</div>

</div>

<?php

$borrarUnidadMedida = new ControladorUnidadMedida();
$borrarUnidadMedida -> ctrBorrarUnidadMedida();

?>


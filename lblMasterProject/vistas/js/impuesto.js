/*=============================================
EDITAR CATEGORIA
=============================================*/
$(".tablas").on("click", ".btnEditarImpuesto", function(){

	var idImpuesto = $(this).attr("idImpuesto");

	var datos = new FormData();
	datos.append("idImpuesto", idImpuesto);

	$.ajax({
		url: "ajax/impuesto.ajax.php",
		method: "POST",
      	data: datos,
      	cache: false,
     	contentType: false,
     	processData: false,
     	dataType:"json",
     	success: function(respuesta){

     		
     		$("#edit_id_impuesto").val(respuesta["id_impuesto"]);
			$("#edit_cod_impuesto").val(respuesta["cod_impuesto"]);
			$("#edit_descr_impuesto").val(respuesta["descr_impuesto"]);
			
			var v_valor_impuesto = Number(respuesta["valor_impuesto"]);
			var v_impuesto_round = Number(v_valor_impuesto).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
			$("#edit_valor_impuesto").val(v_impuesto_round);

			//$("#edit_valor_impuesto").val(respuesta["valor_impuesto"]);
			$("#edit_valor_impuesto").prop("readonly",true);
			 

     	}

	})


})

/*=============================================
ELIMINAR IMPUESTO
=============================================*/
$(".tablas").on("click", ".btnEliminarImpuesto", function(){

	var idImpuesto = $(this).attr("idImpuesto");

	swal({
		title: '¿Está seguro de borrar el impuesto?',
		text: "¡Si no lo está puede cancelar la acción!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Si, borrar impuesto!'
	}).then(function(result){

		if(result.value){

			window.location = "index.php?ruta=impuestos&idImpuesto="+idImpuesto;

		}

	})

})
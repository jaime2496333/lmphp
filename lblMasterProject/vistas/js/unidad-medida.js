/*=============================================
EDITAR UNIDAD DE MEDIDA
=============================================*/
$(".tablas").on("click", ".btnEditarUnidadMedida", function(){

	var idUnidadMedida = $(this).attr("idUnidadMedida");

	var datos = new FormData();
	datos.append("idUnidadMedida", idUnidadMedida);

	$.ajax({
		url: "ajax/unidad-medida.ajax.php",
		method: "POST",
      	data: datos,
      	cache: false,
     	contentType: false,
     	processData: false,
     	dataType:"json",
     	success: function(respuesta){

     		$("#edit_id_unidad_medida").val(respuesta["id_unidad_medida"]);
     		$("#edit_cod_unidad_medida").val(respuesta["cod_unidad_medida"]);
            $("#edit_descr_unidad_medida").val(respuesta["descr_unidad_medida"]);

     	}

	})

})

/*=============================================
ELIMINAR CATEGORIA
=============================================*/
$(".tablas").on("click", ".btnEliminarUnidadMedida", function(){

	 var idUnidadMedida = $(this).attr("idUnidadMedida");

	 swal({
	 	title: '¿Está seguro de borrar la unidad de medida?',
	 	text: "¡Si no lo está puede cancelar la acción!",
	 	type: 'warning',
	 	showCancelButton: true,
	 	confirmButtonColor: '#3085d6',
	 	cancelButtonColor: '#d33',
	 	cancelButtonText: 'Cancelar',
	 	confirmButtonText: 'Si, borrar unidad de medida!'
	 }).then(function(result){

	 	if(result.value){

			window.location = "index.php?ruta=unidad-medida&idUnidadMedida="+idUnidadMedida;

	 	}

	 })

})
-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-06-2019 a las 23:14:31
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `labelmas_sys_inventario`
--
/*CREATE DATABASE IF NOT EXISTS `labelmas_sys_inventario` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `labelmas_sys_inventario`;*/

CREATE DATABASE IF NOT EXISTS `labelmas_sys_inventario` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `labelmas_sys_inventario`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE categorias (
  id INT PRIMARY KEY AUTO_INCREMENT,
  categoria VARCHAR(70) NOT NULL,
  estado INT DEFAULT 1,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) 

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO categorias VALUES
(default, 'Equipos y Repuestos', 1, '2022-02-10 00:00:00'),
(default, 'Etiquetas Transferencia Térmica', 1, '2022-02-10 00:00:00'),
(default, 'Etiquetas Térmica Directa', 1, '2022-02-10 00:00:00'),
(default, 'Etiquetas BOPP', 1, '2022-02-10 00:00:00'),
(default, 'Etiquetas Termicas de Colores', 1, '2022-02-10 00:00:00'),
(default, 'Etiquetas procesadas en imprentas', 1, '2022-02-10 00:00:00'),
(default, 'RIBBONS', 1, '2022-02-10 00:00:00'),
(default, 'Cintas Adhesivas-Plastiflechas-Candados', 1, '2022-02-10 00:00:00'),
(default, 'Otros', 1, '2022-02-10 00:00:00');
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE clientes (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nombre text COLLATE utf8_spanish_ci NOT NULL,
  documento VARCHAR(20) NULL,
  email text COLLATE utf8_spanish_ci NOT NULL,
  telefono text COLLATE utf8_spanish_ci NOT NULL,
  direccion text COLLATE utf8_spanish_ci NOT NULL,
  fecha_nacimiento date NOT NULL,
  compras int(11) NOT NULL,
  ultima_compra datetime NOT NULL,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) 


--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO clientes VALUES
(default, 'Juan Villegas',		'0010101990001A'	, 'juan@hotmail.com', '(300) 341-2345', 'Calle 23 # 45 - 56', '1980-11-02', 7, '2018-02-06 17:47:02', '2018-02-06 22:47:02'),
(default, 'Pedro Pérez',		'0010101990001B'	, 'pedro@gmail.com', '(399) 876-5432', 'Calle 34 N33 -56', '1970-08-07', 7, '2017-12-26 17:27:42', '2017-12-26 22:27:42'),
(default, 'Miguel Murillo',		'0010101990001C'	, 'miguel@hotmail.com', '(254) 545-3446', 'calle 34 # 34 - 23', '1976-03-04', 32, '2017-12-26 17:27:13', '2017-12-27 04:38:13'),
(default, 'Margarita Londoño',	'0010101990001D'	, 'margarita@hotmail.com', '(344) 345-6678', 'Calle 45 # 34 - 56', '1976-11-30', 19, '2019-05-25 01:10:41', '2019-05-25 06:10:41'),
(default, 'Julian Ramirez',		'0010101990001E'	, 'julian@hotmail.com', '(675) 674-5453', 'Carrera 45 # 54 - 56', '1980-04-05', 14, '2017-12-26 17:26:28', '2017-12-26 22:26:28'),
(default, 'Stella Jaramillo',	'0010101990001F'	, 'stella@gmail.com', '(435) 346-3463', 'Carrera 34 # 45- 56', '1956-06-05', 9, '2017-12-26 17:25:55', '2017-12-26 22:25:55'),
(default, 'Eduardo López',		'0010101990001G'	, 'eduardo@gmail.com', '(534) 634-6565', 'Carrera 67 # 45sur', '1978-03-04', 15, '2019-06-20 15:33:23', '2019-06-20 20:33:23'),
(default, 'Ximena Restrepo',	'0010101990001H'	, 'ximena@gmail.com', '(543) 463-4634', 'calle 45 # 23 - 45', '1956-03-04', 18, '2017-12-26 17:25:08', '2017-12-26 22:25:08'),
(default, 'David Guzman',		'0010101990001I'	, 'david@hotmail.com', '(354) 574-5634', 'carrera 45 # 45 ', '1967-05-04', 10, '2017-12-26 17:24:50', '2017-12-26 22:24:50'),
(default, 'Gonzalo Pérez',		'0010101990001J'	, 'gonzalo@yahoo.com', '(235) 346-3464', 'Carrera 34 # 56 - 34', '1967-08-09', 24, '2017-12-25 17:24:24', '2017-12-27 00:30:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE productos (
  id INT PRIMARY KEY AUTO_INCREMENT,
  id_categoria INT NOT NULL,
  id_unidad_medida INT NULL,
  codigo VARCHAR(30) NOT NULL,
  codigo_externo varchar(30) DEFAULT '',
  descripcion text COLLATE utf8_spanish_ci NOT NULL,
  imagen text COLLATE utf8_spanish_ci NOT NULL,
  stock INT NOT NULL,
  precio_compra float NOT NULL,
  precio_venta float NOT NULL,
  ventas INT NOT NULL DEFAULT 0,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) 

--
-- Volcado de datos para la tabla `productos`
--

--categoria equipos y repuestos
INSERT INTO productos VALUES
(default, 1, null, 'DJ0111', 'SC100-1', 'LECTOR CODIGO DE BARRA 3NSTAR TRANSITAR ALAMBRICO', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0112', 'AS10-U', 'LECTOR DE CODIGO DE BARRA UNITECH 1 -D AS10-U', 'vistas/img/productos/default/anonymous.png', 3, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0121', 'ZXP SERIES 3', 'IMPRESORA ZEBRA ZXP SERIE 3  S/N: Z3J153800300', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0122', 'ZT230', 'IMPRESORA ZEBRA CODIGO DE BARRA ZT230', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0123', 'ZM 400', 'IMPRESORA ZEBRA ZM 400', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0124', 'RT 700I', 'IMPRESORA  RT  700I', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0125', 'ZT230', 'IMPRESORA ZT-230 ZEBRA 203 DPI S/N: 52J193001521', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0131', 'ZT410', 'CABEZAL DE IMPRESORA ZT410 203 DPI', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0132', 'ZT410', 'BANDA DE ARRASTRE PARA IMPRESORA ZT410', 'vistas/img/productos/default/anonymous.png', 1, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0133', 'ZT410', 'RODILLO PARA IMPRESORA  ZT410', 'vistas/img/productos/default/anonymous.png', 2, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0134', 'ZT230', 'RODILLO PARA IMPRESORA  ZT230', 'vistas/img/productos/default/anonymous.png', 2, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0141', '', 'PISTOLAS PARA PLASTIFLECHAS', 'vistas/img/productos/default/anonymous.png', 3, 0, 0, 0, '2022-02-10 00:00:00'),
(default, 1, null, 'DJ0142', '', 'KIT DE AGUJAS FINAS PARA PLASTIFLECHAS', 'vistas/img/productos/default/anonymous.png', 4, 0, 0, 0, '2022-02-10 00:00:00');

--etiquetas de transferencia termica
INSERT INTO productos VALUES
(default, 2, null, 'DJ0211TT'	    ,'E-R25'			,'Etiquetas ( 4.3 CM X  3.4 CM) - 2 Filas'    	,'vistas/img/productos/default/anonymous.png', 33.5	 , 0, 0, 0, '2022-02-10 00:00:00'),								
(default, 2, null, 'DJ0212TT'	    ,''					,'Etiquetas 1.25 x 0.75 - 3 Filas'				,'vistas/img/productos/default/anonymous.png', 23	 , 0, 0, 0, '2022-02-10 00:00:00'),										
(default, 2, null, 'DJ0213TT'	    ,''					,'Etiquetas 1.5 x 1 - 2 Filas'					,'vistas/img/productos/default/anonymous.png', 5.5	 , 0, 0, 0, '2022-02-10 00:00:00'),									
(default, 2, null, 'DJ0213TT_E'	,''    				,'Etiquetas 1.5x2.5 1 Filas (Rojas)'			,'vistas/img/productos/default/anonymous.png', 2	 , 0, 0, 0, '2022-02-10 00:00:00'),										
(default, 2, null, 'DJ0214TT'	    ,''					,'Etiquetas 1.75 x 1.5'							,'vistas/img/productos/default/anonymous.png', 20	 , 0, 0, 0, '2022-02-10 00:00:00'),									
(default, 2, null, 'DJ0215TT'	    ,'E-52 2 AL PASO'	,'Etiquetas 2 x 1   2  al Paso '				,'vistas/img/productos/default/anonymous.png', 4.5	 , 0, 0, 0, '2022-02-10 00:00:00'),										
(default, 2, null, 'DJ0213TT_E'	,''    				,'Etiquetas 2 x 2   2  al Paso '				,'vistas/img/productos/default/anonymous.png', 3.73	 , 0, 0, 0, '2022-02-10 00:00:00'),									
(default, 2, null, 'DJ0217TT'	    ,'R-42'				,'Etiquetas 3 x 1 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 7	 , 0, 0, 0, '2022-02-10 00:00:00'),								
(default, 2, null, 'DJ0218TT'	    ,''					,'Etiquetas 3 x 1 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 12	 , 0, 0, 0, '2022-02-10 00:00:00'),							
(default, 2, null, 'DJ0219TT'	    ,''					,'Etiquetas 3 x 1.5 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 14	 , 0, 0, 0, '2022-02-10 00:00:00'),								
(default, 2, null, 'DJ02110TT'	,''    				,'Etiquetas 3 x 2 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 25.75 , 0, 0, 0, '2022-02-10 00:00:00'),			
(default, 2, null, 'DJ02111TT'	,'R-11'				,'Etiquetas 3 x 3 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 13.5	 , 0, 0, 0, '2022-02-10 00:00:00'),			
(default, 2, null, 'DJ02112TT'	,'R-13'				,'Etiquetas 3 cm x 2.4 cm 3 Al Paso (Grande)'	,'vistas/img/productos/default/anonymous.png', 60	 , 0, 0, 0, '2022-02-10 00:00:00'),								
(default, 2, null, 'DJ02113TT'	,''    				,'Etiquetas 3.125 x 1.5 Verde Circular '		,'vistas/img/productos/default/anonymous.png', 1.7	 , 0, 0, 0, '2022-02-10 00:00:00'),							
(default, 2, null, 'DJ02115TT'	,''    				,'Etiquetas 4 x 3 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 8	 , 0, 0, 0, '2022-02-10 00:00:00'),			
(default, 2, null, 'DJ02116TT'	,''    				,'Etiquetas 4 x 4 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 16.5	 , 0, 0, 0, '2022-02-10 00:00:00'),			
(default, 2, null, 'DJ02117TT'	,''    				,'Etiquetas 4 x 6 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 4.0	 , 0, 0, 0, '2022-02-10 00:00:00'),			
(default, 2, null, 'DJ02118TT'	,''    				,'Etiquetas 4 x 6 - 1 Filas (Chocoya)'			,'vistas/img/productos/default/anonymous.png', 5.0	 , 0, 0, 0, '2022-02-10 00:00:00'),						
(default, 2, null, 'DJ02119TT'	,'R-06'				,'Etiquetas 1.563 x 2.344    1 Fila'			,'vistas/img/productos/default/anonymous.png', 14.2	 , 0, 0, 0, '2022-02-10 00:00:00');					

--Etiquetas termica directa
INSERT INTO productos VALUES
(default, 3, null, 'DJ0311TD'     ,''					,'Etiquetas 2 x 1 - 1 Filas'    				,'vistas/img/productos/default/anonymous.png', 20	 , 0, 0, 0, '2022-02-10 00:00:00'),    			
(default, 3, null, 'DJ0312TD'     ,''					,'Etiquetas 4 x 3 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 11	 , 0, 0, 0, '2022-02-10 00:00:00'), 				
(default, 3, null, 'DJ0313TD'     ,''					,'Etiquetas 4 x 4 - 1 Filas'					,'vistas/img/productos/default/anonymous.png', 0.5	 , 0, 0, 0, '2022-02-10 00:00:00'), 					
(default, 3, null, 'DJ0314TD'     ,''					,'Etiquetas Joyeria 2.188 x 0.5'				,'vistas/img/productos/default/anonymous.png', 2.95	 , 0, 0, 0, '2022-02-10 00:00:00'); 	

--Etiquetas BOPP
INSERT INTO productos VALUES
(default, 4, null, 'DJ0411OPP', '', 'Etiquetas 2 x 1 Brillante 2 Filas',	'vistas/img/productos/default/anonymous.png', 20	 , 0, 0, 0, '2022-02-10 00:00:00'), 	
(default, 4, null, 'DJ0412OPP', '', 'Etiquetas 2 x 1 Brillante 2 Filas',	'vistas/img/productos/default/anonymous.png', 11	 , 0, 0, 0, '2022-02-10 00:00:00'), 	
(default, 4, null, 'DJ0413OPP', '', 'Etiquetas 2 x 1 Mate      1 Filas',	'vistas/img/productos/default/anonymous.png', 0.5	 , 0, 0, 0, '2022-02-10 00:00:00'), 	
(default, 4, null, 'DJ0414OPP', '', 'Etiquetas 3 x 1 Mate      1 Filas',	'vistas/img/productos/default/anonymous.png', 2.95	 , 0, 0, 0, '2022-02-10 00:00:00'), 	
(default, 4, null, 'DJ0415OPP', '', 'Etiquetas 4 x 2 Brillante 1 Filas',	'vistas/img/productos/default/anonymous.png', 0		 , 0, 0, 0, '2022-02-10 00:00:00');

--Etiquetas termicas de colores
INSERT INTO productos VALUES
(default, 5, null, 'DJ0511TTC', 'R-04',	'Etiquetas 4 x 4 AMARILLO 1 Filas'			,	'vistas/img/productos/default/anonymous.png', 9.75	 , 0, 0, 0, '2022-02-10 00:00:00'), 	 
(default, 5, null, 'DJ0512TTC', 'R-04',	'Etiquetas 4 x 4 ROJO 1 Filas'				,	'vistas/img/productos/default/anonymous.png', 10	 , 0, 0, 0, '2022-02-10 00:00:00'), 	 			
(default, 5, null, 'DJ0513TTC', 'R-04',	'Etiquetas 4 x 4 VERDE 1 Filas'				,	'vistas/img/productos/default/anonymous.png', 12.9	 , 0, 0, 0, '2022-02-10 00:00:00'), 	 			
(default, 5, null, 'DJ0514TTC', ''	  , 'Etiquetas Verde Fluorecente 1/8  1 Filas'	,	'vistas/img/productos/default/anonymous.png', 2.1	 , 0, 0, 0, '2022-02-10 00:00:00'), 	 						
(default, 5, null, 'DJ0515TTC', ''    , 'Etiquetas Naranja Fluorecente 1/8 1 Filas'	,	'vistas/img/productos/default/anonymous.png', 10	 , 0, 0, 0, '2022-02-10 00:00:00'); 	 									

--Etiquetas procesadas en imprentas
INSERT INTO productos VALUES
(default, 6, null, 'DJ0611EPI', '',	'Etiquetas 4 x 4.5  Full Color',	'vistas/img/productos/default/anonymous.png', 3	 , 0, 0, 0, '2022-02-10 00:00:00');

--RIBBONS
INSERT INTO productos VALUES
(default, 7, null, 'DJ0711RB'	    ,''	,'GF8645AX RIBBONS RESINA  39 MM X 400 MTS  (2 X 400 Mts)'		,	'vistas/img/productos/default/anonymous.png', 64	 , 0, 0, 0, '2022-02-10 00:00:00'), 										
(default, 7, null, 'DJ0712RB'	    ,''	,'RIBOONS RESINA ROJO PEQUEÑO 2.50 x 74 MTS '					,	'vistas/img/productos/default/anonymous.png', 6		 , 0, 0, 0, '2022-02-10 00:00:00'), 	 							
(default, 7, null, 'DJ0715RB'	    ,''	,'RIBBON  CERA ZEBRA 84MM X 300MTS'								,	'vistas/img/productos/default/anonymous.png', 6		 , 0, 0, 0, '2022-02-10 00:00:00'), 	 				
(default, 7, null, 'DJ0716RB'	    ,''	,'RIBBON CERA AZUL 74 X 450mts (2 X 450 )'						,	'vistas/img/productos/default/anonymous.png', 24	 , 0, 0, 0, '2022-02-10 00:00:00'), 	 						
(default, 7, null, 'DJ0717RB'	    ,''	,'B02-114 RIBBON CERA 4.33 pulg x 91 MM'						,	'vistas/img/productos/default/anonymous.png', 2		 , 0, 0, 0, '2022-02-10 00:00:00'), 	 						
(default, 7, null, 'DJ0718RB'	    ,''	,'F.C. RIBBON PLATEADO CERA RESINA FINOTEX 110MM X 500M'		,	'vistas/img/productos/default/anonymous.png', 4	 	 , 0, 0, 0, '2022-02-10 00:00:00'), 	 										
(default, 7, null, 'DJ0719RB_P'   ,''	,'RIBBON CERA AZUL  4.33 X 450mts '								,	'vistas/img/productos/default/anonymous.png', 20	 , 0, 0, 0, '2022-02-10 00:00:00'), 	 				
(default, 7, null, 'DJ0720RB_P'   ,''	,'RIBBON CERA ZEBRA  4.33 X 450mts '							,	'vistas/img/productos/default/anonymous.png', 8		 , 0, 0, 0, '2022-02-10 00:00:00');

--CINTAS ADHESIVAS PLASTIFLECHAS CANDADOS


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `usuario` text COLLATE utf8_spanish_ci NOT NULL,
  `password` text COLLATE utf8_spanish_ci NOT NULL,
  `perfil` text COLLATE utf8_spanish_ci NOT NULL,
  `foto` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1,
  `ultimo_login` datetime,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) 

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `password`, `perfil`, `foto`, `estado`, `ultimo_login`, `fecha`) VALUES
(default, 'Administrador', 'admin', '$2a$07$asxx54ahjppf45sd87a5auXBm1Vr2M1NV5t/zNQtGHGpS5fFirrbG', 'Administrador', 'vistas/img/usuarios/admin/191.jpg', 1, '2019-06-20 16:06:09', '2019-06-20 21:06:09'),
(default, 'Juan Fernando Urrego', 'juan', '$2a$07$asxx54ahjppf45sd87a5auwRi.z6UsW7kVIpm0CUEuCpmsvT2sG6O', 'Vendedor', 'vistas/img/usuarios/juan/461.jpg', 1, '2018-02-06 16:58:50', '2019-06-20 20:28:19'),
(default, 'Julio Gómez', 'julio', '$2a$07$asxx54ahjppf45sd87a5auQhldmFjGsrgUipGlmQgDAcqevQZSAAC', 'Especial', 'vistas/img/usuarios/julio/100.png', 1, '2018-02-06 17:09:22', '2019-05-25 06:06:39'),
(default, 'Ana Gonzalez', 'ana', '$2a$07$asxx54ahjppf45sd87a5auLd2AxYsA/2BbmGKNk2kMChC3oj7V0Ca', 'Vendedor', 'vistas/img/usuarios/ana/260.png', 1, '2017-12-26 19:21:40', '2019-05-25 06:06:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE ventas (
  id int(11) NOT NULL,
  codigo int(11) NOT NULL,
  id_cliente int(11) NOT NULL,
  id_vendedor int(11) NOT NULL, --este es el id del usuario
  productos text COLLATE utf8_spanish_ci NOT NULL,
  impuesto float NOT NULL,
  neto float NOT NULL,
  total float NOT NULL,
  metodo_pago text COLLATE utf8_spanish_ci NOT NULL,
  fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) 

--
-- Volcado de datos para la tabla `ventas iniciales`
--

INSERT INTO ventas VALUES
(17, 10001, 3,  1, '[{\"id\":\"1\",\"descripcion\":\"Aspiradora Industrial \",\"cantidad\":\"2\",\"stock\":\"13\",\"precio\":\"1200\",\"total\":\"2400\"},{\"id\":\"2\",\"descripcion\":\"Plato Flotante para Allanadora\",\"cantidad\":\"2\",\"stock\":\"7\",\"precio\":\"6300\",\"total\":\"12600\"},{\"id\":\"3\",\"descripcion\":\"Compresor de Aire para pintura\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"4200\",\"total\":\"4200\"}]', 3648, 19200, 22848, 'Efectivo', '2018-02-02 01:11:04'),
(18, 10002, 4,  4, '[{\"id\":\"5\",\"descripcion\":\"Cortadora de Piso sin Disco \",\"cantidad\":\"2\",\"stock\":\"18\",\"precio\":\"2156\",\"total\":\"4312\"},{\"id\":\"4\",\"descripcion\":\"Cortadora de Adobe sin Disco \",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"5600\",\"total\":\"5600\"},{\"id\":\"6\",\"descripcion\":\"Disco Punta Diamante \",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"1540\",\"total\":\"1540\"},{\"id\":\"7\",\"descripcion\":\"Extractor de Aire \",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"2156\",\"total\":\"2156\"}]', 2585.52, 13608, 16193.5, 'TC-34346346346', '2018-02-02 14:57:20'),
(19, 10003, 5,  4, '[{\"id\":\"8\",\"descripcion\":\"Guadañadora \",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"2156\",\"total\":\"2156\"},{\"id\":\"9\",\"descripcion\":\"Hidrolavadora Eléctrica \",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"3640\",\"total\":\"3640\"},{\"id\":\"7\",\"descripcion\":\"Extractor de Aire \",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"2156\",\"total\":\"2156\"}]', 1510.88, 7952, 9462.88, 'Efectivo', '2018-01-18 14:57:40'),
(20, 10004, 5,  4, '[{\"id\":\"3\",\"descripcion\":\"Compresor de Aire para pintura\",\"cantidad\":\"5\",\"stock\":\"14\",\"precio\":\"4200\",\"total\":\"21000\"},{\"id\":\"4\",\"descripcion\":\"Cortadora de Adobe sin Disco \",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"5600\",\"total\":\"5600\"},{\"id\":\"5\",\"descripcion\":\"Cortadora de Piso sin Disco \",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"2156\",\"total\":\"2156\"}]', 5463.64, 28756, 34219.6, 'TD-454475467567', '2018-01-25 14:58:09'),
(21, 10005, 6,  2, '[{\"id\":\"4\",\"descripcion\":\"Cortadora de Adobe sin Disco \",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"5600\",\"total\":\"5600\"},{\"id\":\"5\",\"descripcion\":\"Cortadora de Piso sin Disco \",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"2156\",\"total\":\"2156\"},{\"id\":\"3\",\"descripcion\":\"Compresor de Aire para pintura\",\"cantidad\":\"5\",\"stock\":\"9\",\"precio\":\"4200\",\"total\":\"21000\"}]', 5463.64, 28756, 34219.6, 'TC-6756856867', '2018-01-09 14:59:07'),
(22, 10006, 10, 1, '[{\"id\":\"3\",\"descripcion\":\"Compresor de Aire para pintura\",\"cantidad\":\"1\",\"stock\":\"8\",\"precio\":\"4200\",\"total\":\"4200\"},{\"id\":\"4\",\"descripcion\":\"Cortadora de Adobe sin Disco \",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"5600\",\"total\":\"5600\"},{\"id\":\"5\",\"descripcion\":\"Cortadora de Piso sin Disco \",\"cantidad\":\"3\",\"stock\":\"13\",\"precio\":\"2156\",\"total\":\"6468\"},{\"id\":\"6\",\"descripcion\":\"Disco Punta Diamante \",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"1540\",\"total\":\"1540\"}]', 3383.52, 17808, 21191.5, 'Efectivo', '2018-01-26 15:03:22'),
(23, 10007, 9,  1, '[{\"id\":\"6\",\"descripcion\":\"Disco Punta Diamante \",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"1540\",\"total\":\"1540\"},{\"id\":\"7\",\"descripcion\":\"Extractor de Aire \",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"2156\",\"total\":\"2156\"},{\"id\":\"8\",\"descripcion\":\"Guadañadora \",\"cantidad\":\"6\",\"stock\":\"13\",\"precio\":\"2156\",\"total\":\"12936\"},{\"id\":\"9\",\"descripcion\":\"Hidrolavadora Eléctrica \",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"3640\",\"total\":\"3640\"}]', 3851.68, 20272, 24123.7, 'TC-357547467346', '2017-11-30 15:03:53'),
(24, 10008, 12, 1, '[{\"id\":\"2\",\"descripcion\":\"Plato Flotante para Allanadora\",\"cantidad\":\"1\",\"stock\":\"6\",\"precio\":\"6300\",\"total\":\"6300\"},{\"id\":\"7\",\"descripcion\":\"Extractor de Aire \",\"cantidad\":\"5\",\"stock\":\"12\",\"precio\":\"2156\",\"total\":\"10780\"},{\"id\":\"6\",\"descripcion\":\"Disco Punta Diamante \",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"1540\",\"total\":\"1540\"},{\"id\":\"9\",\"descripcion\":\"Hidrolavadora Eléctrica \",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"3640\",\"total\":\"3640\"}]', 4229.4, 22260, 26489.4, 'TD-35745575', '2017-12-25 15:04:11'),
(25, 10009, 11, 1, '[{\"id\":\"10\",\"descripcion\":\"Hidrolavadora Gasolina\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"3094\",\"total\":\"3094\"},{\"id\":\"9\",\"descripcion\":\"Hidrolavadora Eléctrica \",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"3640\",\"total\":\"3640\"},{\"id\":\"6\",\"descripcion\":\"Disco Punta Diamante \",\"cantidad\":\"1\",\"stock\":\"15\",\"precio\":\"1540\",\"total\":\"1540\"}]', 1572.06, 8274, 9846.06, 'TD-5745745745', '2017-08-15 15:04:38'),
(26, 10010, 8,  1, '[{\"id\":\"9\",\"descripcion\":\"Hidrolavadora Eléctrica \",\"cantidad\":\"1\",\"stock\":\"15\",\"precio\":\"3640\",\"total\":\"3640\"},{\"id\":\"10\",\"descripcion\":\"Hidrolavadora Gasolina\",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"3094\",\"total\":\"3094\"}]', 1279.46, 6734, 8013.46, 'Efectivo', '2017-12-07 15:05:09'),
(27, 10011, 7,  1, '[{\"id\":\"60\",\"descripcion\":\"Cortadora de Baldosin\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"1302\",\"total\":\"1302\"},{\"id\":\"59\",\"descripcion\":\"Cono slump\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"196\",\"total\":\"196\"},{\"id\":\"58\",\"descripcion\":\"Coche llanta neumatica\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"588\",\"total\":\"588\"},{\"id\":\"57\",\"descripcion\":\"Cizalla de Tijera\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"812\",\"total\":\"812\"}]', 550.62, 2898, 3448.62, 'Efectivo', '2017-12-25 22:23:38'),
(28, 10012, 12, 2, '[{\"id\":\"59\",\"descripcion\":\"Cono slump\",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"196\",\"total\":\"196\"},{\"id\":\"58\",\"descripcion\":\"Coche llanta neumatica\",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"588\",\"total\":\"588\"},{\"id\":\"54\",\"descripcion\":\"Chapeta\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"924\",\"total\":\"924\"},{\"id\":\"53\",\"descripcion\":\"Bomba Hidrostatica\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"1078\",\"total\":\"1078\"}]', 529.34, 2786, 3315.34, 'TC-3545235235', '2017-12-25 22:24:24'),
(29, 10013, 11, 2, '[{\"id\":\"54\",\"descripcion\":\"Chapeta\",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"924\",\"total\":\"924\"},{\"id\":\"59\",\"descripcion\":\"Cono slump\",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"196\",\"total\":\"196\"},{\"id\":\"60\",\"descripcion\":\"Cortadora de Baldosin\",\"cantidad\":\"5\",\"stock\":\"14\",\"precio\":\"1302\",\"total\":\"6510\"}]', 1449.7, 7630, 9079.7, 'TC-425235235235', '2017-12-26 22:24:50'),
(30, 10014, 10, 2, '[{\"id\":\"59\",\"descripcion\":\"Cono slump\",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"196\",\"total\":\"196\"},{\"id\":\"54\",\"descripcion\":\"Chapeta\",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"924\",\"total\":\"924\"},{\"id\":\"53\",\"descripcion\":\"Bomba Hidrostatica\",\"cantidad\":\"10\",\"stock\":\"9\",\"precio\":\"1078\",\"total\":\"10780\"}]', 2261, 11900, 14161, 'Efectivo', '2017-12-26 22:25:09'),
(31, 10015, 9,  2, '[{\"id\":\"57\",\"descripcion\":\"Cizalla de Tijera\",\"cantidad\":\"3\",\"stock\":\"16\",\"precio\":\"812\",\"total\":\"2436\"}]', 462.84, 2436, 2898.84, 'Efectivo', '2017-12-26 22:25:33'),
(32, 10016, 8,  2, '[{\"id\":\"58\",\"descripcion\":\"Coche llanta neumatica\",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"588\",\"total\":\"588\"},{\"id\":\"57\",\"descripcion\":\"Cizalla de Tijera\",\"cantidad\":\"5\",\"stock\":\"11\",\"precio\":\"812\",\"total\":\"4060\"},{\"id\":\"56\",\"descripcion\":\"Cizalla de Palanca\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"630\",\"total\":\"630\"}]', 1002.82, 5278, 6280.82, 'TD-4523523523', '2017-12-26 22:25:55'),
(33, 10017, 7,  2, '[{\"id\":\"57\",\"descripcion\":\"Cizalla de Tijera\",\"cantidad\":\"4\",\"stock\":\"7\",\"precio\":\"812\",\"total\":\"3248\"},{\"id\":\"52\",\"descripcion\":\"Bascula \",\"cantidad\":\"3\",\"stock\":\"17\",\"precio\":\"182\",\"total\":\"546\"},{\"id\":\"55\",\"descripcion\":\"Cilindro muestra de concreto\",\"cantidad\":\"2\",\"stock\":\"18\",\"precio\":\"560\",\"total\":\"1120\"},{\"id\":\"56\",\"descripcion\":\"Cizalla de Palanca\",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"630\",\"total\":\"630\"}]', 1053.36, 5544, 6597.36, 'Efectivo', '2017-12-26 22:26:28'),
(34, 10018, 6,  2, '[{\"id\":\"51\",\"descripcion\":\"Tensor\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"140\",\"total\":\"140\"},{\"id\":\"52\",\"descripcion\":\"Bascula \",\"cantidad\":\"5\",\"stock\":\"12\",\"precio\":\"182\",\"total\":\"910\"},{\"id\":\"53\",\"descripcion\":\"Bomba Hidrostatica\",\"cantidad\":\"1\",\"stock\":\"8\",\"precio\":\"1078\",\"total\":\"1078\"}]', 404.32, 2128, 2532.32, 'Efectivo', '2017-12-26 22:26:51'),
(35, 10019, 5,  2, '[{\"id\":\"56\",\"descripcion\":\"Cizalla de Palanca\",\"cantidad\":\"15\",\"stock\":\"3\",\"precio\":\"630\",\"total\":\"9450\"},{\"id\":\"55\",\"descripcion\":\"Cilindro muestra de concreto\",\"cantidad\":\"1\",\"stock\":\"17\",\"precio\":\"560\",\"total\":\"560\"}]', 1901.9, 10010, 11911.9, 'Efectivo', '2017-12-26 22:27:13'),
(36, 10020, 4,  2, '[{\"id\":\"55\",\"descripcion\":\"Cilindro muestra de concreto\",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"560\",\"total\":\"560\"},{\"id\":\"54\",\"descripcion\":\"Chapeta\",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"924\",\"total\":\"924\"}]', 281.96, 1484, 1765.96, 'TC-46346346346', '2017-12-26 22:27:42'),
(37, 10021, 3,  1, '[{\"id\":\"60\",\"descripcion\":\"Cortadora de Baldosin\",\"cantidad\":\"1\",\"stock\":\"13\",\"precio\":\"1302\",\"total\":\"1302\"},{\"id\":\"59\",\"descripcion\":\"Cono slump\",\"cantidad\":\"1\",\"stock\":\"15\",\"precio\":\"196\",\"total\":\"196\"}]', 149.8, 1498, 1647.8, 'Efectivo', '2018-02-06 22:47:02'),
(38, 10022, 6,  1, '[{\"id\":\"60\",\"descripcion\":\"Cortadora de Baldosin\",\"cantidad\":\"1\",\"stock\":\"12\",\"precio\":\"1302\",\"total\":\"1302\"},{\"id\":\"59\",\"descripcion\":\"Cono slump\",\"cantidad\":\"1\",\"stock\":\"14\",\"precio\":\"196\",\"total\":\"196\"},{\"id\":\"58\",\"descripcion\":\"Coche llanta neumatica\",\"cantidad\":\"1\",\"stock\":\"16\",\"precio\":\"588\",\"total\":\"588\"},{\"id\":\"57\",\"descripcion\":\"Cizalla de Tijera\",\"cantidad\":\"1\",\"stock\":\"19\",\"precio\":\"812\",\"total\":\"812\"},{\"id\":\"56\",\"descripcion\":\"Cizalla de Palanca\",\"cantidad\":\"1\",\"stock\":\"2\",\"precio\":\"630\",\"total\":\"630\"}]', 141.12, 3528, 3669.12, 'Efectivo', '2019-05-25 06:10:41'),
(39, 10023, 9,  1, '[{\"id\":\"59\",\"descripcion\":\"Cono slump\",\"cantidad\":\"1\",\"stock\":\"13\",\"precio\":\"196\",\"total\":\"196\"},{\"id\":\"60\",\"descripcion\":\"Cortadora de Baldosin\",\"cantidad\":\"1\",\"stock\":\"11\",\"precio\":\"1302\",\"total\":\"1302\"},{\"id\":\"57\",\"descripcion\":\"Cizalla de Tijera\",\"cantidad\":\"1\",\"stock\":\"18\",\"precio\":\"812\",\"total\":\"812\"}]', 277.2, 2310, 2587.2, 'Efectivo', '2019-06-20 20:33:23');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



--TABLA DE IMPUESTOS
CREATE TABLE impuestos(
id_impuesto INT PRIMARY KEY AUTO_INCREMENT,
cod_impuesto VARCHAR(20) NOT NULL,
descr_impuesto VARCHAR(50) NOT NULL,
valor_impuesto decimal(10,5),
estado_impuesto INT NOT NULL DEFAULT 1,
usuario_registro INT NOT NULL
)

--valor impuesto
INSERT INTO `impuestos`(`cod_impuesto`, `descr_impuesto`, `valor_impuesto`, `estado_impuesto`, `usuario_registro`) VALUES ('IVA','Impuesto al Valor agregado',15,1,1)

--TABLA DE UNIDADES DE MEDIDA
CREATE TABLE unidad_medida(
id_unidad_medida INT PRIMARY KEY AUTO_INCREMENT,
cod_unidad_medida VARCHAR(20) NOT NULL,
descr_unidad_medida VARCHAR(50) NOT NULL,
estado_um INT NOT NULL DEFAULT 1,
usuario_registro INT NOT NULL
)

--UNIDADES DE MEDIDA IDENTIFICADAS
INSERT INTO `unidad_medida`(`id_unidad_medida`, `cod_unidad_medida`, `descr_unidad_medida`, `estado_um`, `usuario_registro`) VALUES
 (default, 'und','Unidad',1,1),
 (default, 'rol/mil','Rollo/Millar',1,1),
 (default, 'rollo','Rollo',1,1);
 





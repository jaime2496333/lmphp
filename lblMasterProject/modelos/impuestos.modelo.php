<?php

require_once "conexion.php";

class ModeloImpuestos{

    /*MOSTRAR IMPUESTOS*/
    static public function mdlMostrarImpuestos($tabla, $item, $valor){

        if($item != null){

            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ");

            $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

            $stmt -> execute();

            return $stmt -> fetch();

        }else{

            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

        }

        $stmt -> close();

		$stmt = null;

    }


    /*=============================================
	CREAR IMPUESTO
	=============================================*/

	static public function mdlIngresarImpuesto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(cod_impuesto, descr_impuesto, valor_impuesto, estado_impuesto, usuario_registro) VALUES (:cod_impuesto, :descr_impuesto, :valor_impuesto, 1, :usuario_registro)");       

	    $stmt->bindParam("cod_impuesto", $datos["cod_impuesto"], PDO::PARAM_INT);
		$stmt->bindParam("descr_impuesto", $datos["descr_impuesto"], PDO::PARAM_STR);
        $stmt->bindParam("valor_impuesto", $datos["valor_impuesto"], PDO::PARAM_STR);
        $stmt->bindParam("usuario_registro", $datos["usuario_registro"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	EDITAR IMPUESTO
	=============================================*/

	static public function mdlEditarImpuesto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET descr_impuesto = :edit_descr_impuesto, cod_impuesto = :edit_cod_impuesto WHERE id_impuesto = :edit_id_impuesto");

		$stmt -> bindParam(":edit_descr_impuesto", $datos["descr_impuesto"], PDO::PARAM_STR);
		$stmt -> bindParam(":edit_id_impuesto", $datos["id_impuesto"], PDO::PARAM_INT);
		$stmt -> bindParam(":edit_cod_impuesto", $datos["cod_impuesto"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	BORRAR IMPUESTO
	=============================================*/

	static public function mdlBorrarImpuesto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE IMPUESTOS SET ESTADO_IMPUESTO = 2 WHERE id_impuesto = :edit_id_impuesto ");

		$stmt -> bindParam(":edit_id_impuesto", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}


    

}
<?php

require_once "conexion.php";

class ModeloUnidadMedida{

    /*MOSTRAR UNIDAD DE MEDIDA*/
    static public function mdlMostrarUnidadMedida($tabla, $item, $valor){

        if($item != null){

            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item ");

            $stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

            $stmt -> execute();

            return $stmt -> fetch();

        }else{

            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

        }

        $stmt -> close();

		$stmt = null;

    }

    /*=============================================
	CREAR UNIDAD DE MEDIDA
	=============================================*/

	static public function mdlIngresarUnidadMedida($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(cod_unidad_medida, descr_unidad_medida, estado_um, usuario_registro) VALUES (:cod_unidad_medida, :descr_unidad_medida, 1, :usuario_registro)");       

	    $stmt->bindParam("cod_unidad_medida", $datos["cod_unidad_medida"], PDO::PARAM_STR);
		$stmt->bindParam("descr_unidad_medida", $datos["descr_unidad_medida"], PDO::PARAM_STR);
        $stmt->bindParam("usuario_registro", $datos["usuario_registro"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "Ocurrió un error al intentar guardar la unidad de medida.";
		
		}
		

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	EDITAR UNIDAD DE MEDIDA
	=============================================*/

	static public function mdlEditarUnidadMedida($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET descr_unidad_medida = :edit_descr_unidad_medida, cod_unidad_medida = :edit_cod_unidad_medida WHERE id_unidad_medida = :edit_id_unidad_medida");

		$stmt -> bindParam(":edit_descr_unidad_medida", $datos["descr_unidad_medida"], PDO::PARAM_STR);
		$stmt -> bindParam(":edit_id_unidad_medida", $datos["id_unidad_medida"], PDO::PARAM_INT);
		$stmt -> bindParam(":edit_cod_unidad_medida", $datos["cod_unidad_medida"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "Ocurrió un error al intentar editar la unidad de medida.";
		
		}

		$stmt->close();
		$stmt = null;

	}

	/*=============================================
	BORRAR UNIDAD DE MEDIDA
	=============================================*/

	static public function mdlBorrarUnidadMedida($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado_um = 2 WHERE id_unidad_medida = :id_unidad_medida");

		$stmt -> bindParam(":id_unidad_medida", $datos, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "Ocurrió un error al intentar eliminar la unidad de medida.";

		}

		$stmt -> close();

		$stmt = null;

	}

    

}